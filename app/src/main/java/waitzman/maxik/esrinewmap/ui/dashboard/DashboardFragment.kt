package waitzman.maxik.esrinewmap.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import waitzman.maxik.esrinewmap.R
/*
import com.esri.arcgisruntime.mapping.view.MapView
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
*/





class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    //private var mMapView: MapView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(this, Observer {
            textView.text = it
        })
        //mMapView = root.findViewById(R.id.mapView)
        setupMap()

        return root
    }
    override fun onPause() {
        /*if (mMapView != null) {
            mMapView?.pause()
        }*/
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        /*if (mMapView != null) {
            mMapView?.resume()
        }*/
    }

    override fun onDestroy() {
        /*if (mMapView != null) {
            mMapView?.dispose()
        }*/
        super.onDestroy()
    }


    private fun setupMap() {
        /*if (mMapView != null) {
            val basemapType = Basemap.Type.STREETS_VECTOR
            val latitude = 34.09042
            val longitude = -118.71511
            val levelOfDetail = 11
            val map = ArcGISMap(basemapType, latitude, longitude, levelOfDetail)
            mMapView?.map = map
        }*/
    }

}
